//
//  ShadowButton.swift
//  CoolApp
//
//  Created by Stern Eduard on 29/07/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import UIKit

class ShadowButton: UIButton {

    override func awakeFromNib() {
        setupView()
    }
    
    func setupView () {
        self.layer.borderColor = UIColor.darkGray.cgColor
        self.layer.borderWidth = 5.0
        self.layer.shadowRadius = 10.0
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize.zero
    }
}
