//
//  RestaurantTableViewCell.swift
//  CoolApp
//
//  Created by Stern Eduard on 30/07/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import UIKit

class RestaurantTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
