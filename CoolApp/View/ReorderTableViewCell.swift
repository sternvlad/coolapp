//
//  ReorderTableViewCell.swift
//  CoolApp
//
//  Created by Stern Eduard on 29/07/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import UIKit

class ReorderTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var weConsLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var sugestedOrderLbl: UILabel!
    @IBOutlet weak var isOnOrder: UISwitch!
    @IBOutlet weak var quantityTF: UITextField!
    @IBOutlet weak var btnQuantity: UIButton!
    @IBOutlet weak var switchOrder: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
