//
//  Alertable.swift
//  Uber Clone Dev
//
//  Created by Stern Eduard on 11/07/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import Foundation
import UIKit

protocol Alertable {}

extension Alertable where Self: UIViewController {
    func showAlert (withTittle title: String,  message: String) {
        let alertController = UIAlertController (title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction (title: "OK", style: .default, handler: nil)
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
    }
    func showAlert (_ message: String) {
        let alertController = UIAlertController (title: "Error", message: message, preferredStyle: .alert)
        let action = UIAlertAction (title: "OK", style: .default, handler: nil)
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
    }
}
