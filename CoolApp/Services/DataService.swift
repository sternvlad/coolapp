//
//  DataService.swift
//  Boost
//
//  Created by Stern Eduard on 17/07/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import Foundation
import FirebaseFirestore

let db = Firestore.firestore()
typealias CompletionHandlerRestaurants = (_ restaurant:Restaurant?) -> Void
typealias CompletionHandlerOrders = (_ order:[Order?]) -> Void
typealias CompletionHandlerProductOrders = (_ order:[ProductOrder?]) -> Void

class DataService {
    static let instance = DataService ()
    
    private var _REF_BASE = db
    private var _REF_USERS = db.collection("users")
    private var _REF_RESTAURANTS = db.collection("restaurants")
    private var _REF_PRODUCTS = db.collection("Products")
    private var _REF_ORDERS = db.collection("Orders")
    private var _REF_PRODUCT_ORDERS = db.collection("ProductOrders")
    
    var REF_BASE: Firestore {
        return _REF_BASE
    }
    
    var REF_USERS: CollectionReference {
        return _REF_USERS
    }
    
    var REF_RESTAURANTS: CollectionReference {
        return _REF_RESTAURANTS
    }
    
    var REF_PRODUCTS: CollectionReference {
        return _REF_PRODUCTS
    }
    
    var REF_ORDERS: CollectionReference {
        return _REF_ORDERS
    }
    
    var REF_PRODUCTORDERS: CollectionReference {
        return _REF_PRODUCT_ORDERS
    }
    
    func createFirebaseDBUser (uid: String, userData: Dictionary<String, Any>) {
        REF_USERS.document(uid).setData(userData)
    }
    
    func createRestaurant (uid: String, restData: Dictionary<String, Any>) {
        REF_RESTAURANTS.document(uid).setData(restData)
    }
    
    func createProduct (prodData: Dictionary<String, Any>) {
        REF_PRODUCTS.document().setData(prodData)
    }
    
    func createOrder (uid:String, orderData: Dictionary<String, Any>) {
        REF_ORDERS.document(uid).setData(orderData)
    }
    
    func createProductOrder (prodOrderData: Dictionary<String,Any>) {
        REF_PRODUCTORDERS.document().setData(prodOrderData)
    }
    
    func loadRestaurantWithId (selectedRestaurantId: String, completionHandler: @escaping CompletionHandlerRestaurants) {
        var restaurant: Restaurant? = nil
        REF_RESTAURANTS.document(selectedRestaurantId).getDocument { (querySnapshot, error) in
            if let querySnap = querySnapshot?.data() {
            
                let restaurantName = querySnap["name"] as! String
                let restaurantCity = querySnap["city"] as! String
                let restaurantCountry = querySnap["country"] as! String
            
                restaurant = Restaurant(id: querySnapshot?.documentID ?? "", name: restaurantName, city: restaurantCity, country: restaurantCountry)
                completionHandler(restaurant)
            }
        }
    }
    
    func loadOrdersWithUserIdAndRestaurantId (userId: String, restaurantId: String, completionHandler: @escaping CompletionHandlerOrders) {
        var orders = [Order?]()
        DataService.instance.REF_ORDERS.getDocuments {(querySnapshot, error) in
            
            if let orderSnapshots = querySnapshot?.documents {
                for order in orderSnapshots {
                    let orderID = order.documentID
                    let orderRestID = order.data()["restaurantId"] as! String
                    let userID = order.data()["userId"] as! String
                    let status = order.data()["status"] as! String
                    let timestamp = order.data()["date"] as! Timestamp
                    let date = Date(timeIntervalSince1970: TimeInterval(timestamp.seconds))
                    if userID == userId && restaurantId == restaurantId {
                        orders.append(Order(orderId: orderID, restaurantId: orderRestID, userId: userID, status: status, products: [], date: date))
                       
                    }
                }
                completionHandler (orders.sorted(by: { $0?.date.compare($1!.date) == .orderedAscending }))
            }
        }
    }
    
    func loadProductsByOrder (orderID: String, completionHandler:@escaping CompletionHandlerProductOrders) {
        var products = [ProductOrder?]()
        DataService.instance.REF_PRODUCTORDERS.getDocuments(completion: { (snapProds, error) in
            if let prodSnap = snapProds?.documents {
                for prod in prodSnap {
                    let orderId = prod.data()["orderId"] as! String
                    if (orderID == orderId) {
                        let id = prod.documentID
                        let name = prod.data()["productName"] as! String
                        let productId = prod.data()["productId"] as! String
                        let quantity = "\(String(describing: prod.data()["quantity"] ?? ""))"
                        products.append(ProductOrder(id: id, name: name, quantity: quantity, productId: productId, orderId: orderId))
                    }
                }
            }
            completionHandler (products)
        })
    }
}
