//
//  ViewController.swift
//  CoolApp
//
//  Created by Stern Eduard on 29/07/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import UIKit
import FirebaseAuth
import JGProgressHUD

class LoginViewController: UIViewController, Alertable {

    @IBOutlet weak var usernameTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    let hud = JGProgressHUD(style: .dark)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.moveViewWhenKeyboardShows()
        usernameTxt.delegate = self
        passwordTxt.delegate = self
        hud.textLabel.text = "Loading"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.usernameTxt.text = ""
        self.passwordTxt.text = ""
    }

    @IBAction func loginButtonPressed(_ sender: Any) {
        guard let emailAddress = usernameTxt.text else {return}
        guard let password = passwordTxt.text else {return}
        if !emailAddress.isValidEmail() {
            self.showAlert("Email invalid. Please try again")
            return
        }
        if password.count < 8 {
            self.showAlert("Your password is wrong")
            return
        }
        
        hud.show(in: self.view)
        Auth.auth().signIn(withEmail: emailAddress, password: password) { [weak self] (authDataResult, error) in
            guard let strongSelf = self else {
                return
            }
            if error == nil {
                DataService.instance.REF_USERS.document((authDataResult?.user.uid)!).getDocument(completion: { (documentSnapshot, error) in
                    if let querySnap = documentSnapshot?.data() {
                        let role = querySnap["role"] as! String
                        let userId = documentSnapshot?.documentID
                        UserDefaults.standard.set(userId, forKey: "userId")
                        UserDefaults.standard.set(role, forKey: "role")
                        if (role == "admin"){
                            self?.performSegue(withIdentifier: "goToRestaurantList", sender: self)
                        }else {
                            self?.performSegue(withIdentifier: "segueLogin", sender: self)
                        }
                    }else {
                        self?.performSegue(withIdentifier: "segueLogin", sender: self)
                    }
                    strongSelf.hud.dismiss()
                })
            }else {
                strongSelf.hud.dismiss()
                if let errorCode = AuthErrorCode (rawValue: error!._code) {
                    switch errorCode {
                    case .wrongPassword:
                        print ("That was a wrong password")
                        strongSelf.showAlert("That was a wrong password")
                        return;
                    case .invalidEmail:
                        print ("Email invalid. Please try again")
                        strongSelf.showAlert("Email invalid. Please try again")
                        return;
                    default:
                        print ("There was an error. Please try again")
                        strongSelf.showAlert("There was an error. Please try again")
                    }
                }
            }
        }
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == usernameTxt) {
            passwordTxt.becomeFirstResponder()
        }
        if (textField == passwordTxt) {
            view.endEditing(true) // change to login
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
}

