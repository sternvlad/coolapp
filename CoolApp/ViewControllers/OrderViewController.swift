//
//  OrderViewController.swift
//  CoolApp
//
//  Created by Stern Eduard on 29/07/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import UIKit
import SearchTextField

class OrderViewController: UIViewController {

    @IBOutlet weak var pickerView: UIStackView!
    @IBOutlet weak var restaurantNameTxt: SearchTextField!
    @IBOutlet weak var dateTxt: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var selectRestaurantBtn: ShadowButton!
    var restaurants: [Restaurant] = []
    var restaurantNames: [SearchTextFieldItem] = []
    var restaurantNamesStr : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.moveViewWhenKeyboardShows()
        restaurantNameTxt.delegate = self
        self.pickerView.frame = CGRect.init(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: 502)
        setupSearch()
        // Do any additional setup after loading the view.
    }
    
    func setupSearch () {
        DataService.instance.REF_RESTAURANTS.getDocuments { [weak self] (querySnapshot, error) in
            guard let strongSelf = self else {return}
            
            if let restaurantSnapshots = querySnapshot?.documents {
                for restaurant in restaurantSnapshots {
                    let restaurantName = restaurant.data()["name"] as! String
                    let restaurantCity = restaurant.data()["city"] as! String
                    let restaurantCountry = restaurant.data()["country"] as! String
                    strongSelf.restaurantNames.append(SearchTextFieldItem (title: restaurantName))
                    strongSelf.restaurantNamesStr.append(restaurantName)
                    strongSelf.restaurants.append(Restaurant (id: restaurant.documentID, name: restaurantName, city: restaurantCity, country: restaurantCountry))
                }
            }
            strongSelf.restaurantNameTxt.filterItems(strongSelf.restaurantNames)
        }
        restaurantNameTxt.theme.font = UIFont.systemFont(ofSize: 14)
        restaurantNameTxt.theme.bgColor = UIColor.white
        restaurantNameTxt.theme.borderColor = UIColor.darkGray
        restaurantNameTxt.theme.separatorColor = UIColor.lightGray
        restaurantNameTxt.theme.cellHeight = 50
    }
    

    @IBAction func dateBtnPressed(_ sender: Any) {
        UIView.animate(withDuration: 0.5) {
            self.pickerView.frame = CGRect.init(x: 0, y: self.view.frame.height-266, width: self.view.frame.width, height: 266)
        }
    }
    
    
    @IBAction func cancelBtnPressed(_ sender: Any) {
        UIView.animate(withDuration: 0.5) {
            self.pickerView.frame = CGRect.init(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: 502)
        }
    }
    
    @IBAction func selectRestaurantPressed(_ sender: Any) {
        performSegue(withIdentifier: "reorderSegue", sender: self)
    }
    
    @IBAction func doneToolbarBtnPressed(_ sender: Any) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let dateString = formatter.string(from: datePicker.date)
        dateTxt.text = dateString
        UIView.animate(withDuration: 0.5) {
            self.pickerView.frame = CGRect.init(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: 502)
        }
    }
}

extension OrderViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickerView.frame = CGRect.init(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: 502)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true) // change to login
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text {
            if (restaurantNamesStr.contains(text)) {
                selectRestaurantBtn.isHidden = false
                for rest in restaurants {
                    if (rest.name == text) {
                        UserDefaults.standard.set(rest.id, forKey: "selectedRestaurantID")
                    }
                }
            }else {
                selectRestaurantBtn.isHidden = true
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text {
            if (restaurantNamesStr.contains(text)) {
                selectRestaurantBtn.isHidden = false
                for rest in restaurants {
                    if (rest.name == text) {
                        UserDefaults.standard.set(rest.id, forKey: "selectedRestaurantID")
                    }
                }
            }else {
                selectRestaurantBtn.isHidden = true
            }
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
}
