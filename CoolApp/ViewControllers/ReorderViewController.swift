//
//  ReorderViewController.swift
//  CoolApp
//
//  Created by Stern Eduard on 29/07/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import UIKit
import SideMenuSwift

class ReorderViewController: UIViewController, Alertable {
    
    @IBOutlet weak var pickerView: UIStackView!
    @IBOutlet weak var dateTxt: UITextField!
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var restaurantNameLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var restaurantIdLbl: UILabel!
    
    var products: [Product] = []
    var orderedProducts: [Product] = []
    var selectedRestaurantId = ""
    var selectedRestaurant: Restaurant?
    var numberOfRowsInPickerView = 10;
    var selectedRow = -1
    var resetOrder = false
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.pickerView.frame = CGRect.init(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: 502)
        self.picker.backgroundColor = UIColor.white
        SideMenuController.preferences.basic.menuWidth = 480
        selectedRestaurantId = UserDefaults.standard.string(forKey: "selectedRestaurantID") ?? ""
        picker.delegate = self
        DataService.instance.loadRestaurantWithId(selectedRestaurantId: selectedRestaurantId) { [weak self] (restaurant) in
            if let restaurant = restaurant {
                self?.selectedRestaurant = restaurant
                self?.restaurantIdLbl.text = "Restaurant ID: \(restaurant.id)"
                self?.locationLbl.text = "\(restaurant.city) -- \(restaurant.country)"
                self?.restaurantNameLbl.text = "\(restaurant.name)"
            }else {
                self?.selectedRestaurant = nil
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadProductsFromDatabase()
    }
    
    
    func loadProductsFromDatabase () {
        self.products.removeAll()
        DataService.instance.REF_PRODUCTS.getDocuments { [weak self] (querySnapshot, error) in
            guard let strongSelf = self else {return}
            
            if let productsSnapshots = querySnapshot?.documents {
                for product in productsSnapshots {
                    let productId = product.documentID
                    let productName = product.data()["name"] as! String
                    let prod6WeCons = "\(String(describing: product.data()["6weCons"] ?? ""))"
                    let prodQ = "\(String(describing: product.data()["quantity"] ?? ""))"
                    let resID = "\(String(describing: product.data()["restaurantId"] ?? ""))"
                    let sugestedOrder = "\(String(describing: product.data()["sugestedOrder"] ?? ""))"
                    if resID == strongSelf.selectedRestaurantId || strongSelf.selectedRestaurantId == "" {
                        strongSelf.products.append(Product (id: productId, name: productName, quantity: prodQ, sixWeCons: prod6WeCons, restId: resID, sugestedOrder: sugestedOrder))
                    }
                }
            }
            strongSelf.orderedProducts = strongSelf.products
            strongSelf.tableView.reloadData()
        }
    }
    
    @IBAction func menuButtonDidClicked(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    
    @IBAction func showPicker(_ sender: Any) {
        selectedRow = (sender as! UIButton).tag
        let prod = products[selectedRow] as Product
        numberOfRowsInPickerView = Int (prod.quantity)! + 1
        UIView.animate(withDuration: 0.5) {
            self.pickerView.frame = CGRect.init(x: 0, y: self.view.frame.height-266, width: self.view.frame.width, height: 266)
        }
    }
    
    
    @IBAction func cancelBtnPressed(_ sender: Any) {
        //picker.selectRow(0, inComponent: 0, animated: false)
        UIView.animate(withDuration: 0.5) {
            self.pickerView.frame = CGRect.init(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: 502)
        }
    }
    
    
    @IBAction func doneToolbarBtnPressed(_ sender: Any) {
        orderedProducts[selectedRow].orderedQuantity = picker.selectedRow(inComponent: 0)
        let indexPath = IndexPath (item: selectedRow, section: 0)
        self.tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
        UIView.animate(withDuration: 0.5) {
            self.pickerView.frame = CGRect.init(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: 502)
        }
    }
    
    @IBAction func switchOrder(_ sender: Any) {
        let tag = (sender as! UISwitch).tag
        if (sender as! UISwitch).isOn {
            orderedProducts[tag].productChecked = true
        }else {
            orderedProducts[tag].productChecked = false
        }
    }
    
    @IBAction func placeOrderBtnPressed(_ sender: Any) {
        var noProducts = true
        let orderId = UUID().uuidString
        let orderData = ["restaurantId" : selectedRestaurant?.id ?? "",
                         "status" : "received",
                         "date" : Date.init(),
                         "userId" : UserDefaults.standard.string(forKey: "userId") ?? ""] as [String : Any]
        for prod in orderedProducts {
            if prod.productChecked == true && prod.orderedQuantity != 0 {
                noProducts = false
                let orderProdData = ["orderId": orderId,
                                     "productId" : prod.id,
                                     "productName" : prod.name,
                                     "quantity" : prod.orderedQuantity] as [String : Any]
                DataService.instance.createProductOrder(prodOrderData: orderProdData)
            }
        }
        if (noProducts) {
            showAlert("There's nothing in your shopping bag")
        }else {
            DataService.instance.createOrder(uid: orderId, orderData: orderData as Dictionary<String, Any>)
            resetOrder = true
            orderedProducts = products
            self.tableView.reloadData()
            resetOrder = false
            showAlert(withTittle: "Thank you", message: "Your order was sent, you can see the status of your order in orders history")
        }
    }
    
}


extension ReorderViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderTableCell") as! ReorderTableViewCell
        let prod = orderedProducts[indexPath.row]
        cell.nameLbl.text = prod.name
        cell.weConsLbl.text = "\(prod.sixWeCons)"
        cell.quantityLbl.text = "\(prod.quantity)"
        cell.sugestedOrderLbl.text = "\(prod.sugestedOrder)"
        cell.btnQuantity.tag = indexPath.row
        cell.quantityTF.text = "\(prod.orderedQuantity)"
        cell.switchOrder.tag = indexPath.row
        cell.switchOrder.isOn = prod.productChecked
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

extension ReorderViewController: UIPickerViewDelegate, UIPickerViewDataSource {
   
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return numberOfRowsInPickerView
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(row)"
    }
}
