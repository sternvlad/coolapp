//
//  AddRestaurantViewController.swift
//  CoolApp
//
//  Created by Stern Eduard on 31/07/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import UIKit

class AddRestaurantViewController: UIViewController {

    @IBOutlet weak var cityTxt: UITextField!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var countryTxt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.moveViewWhenKeyboardShows()
        cityTxt.delegate = self
        nameTxt.delegate = self
        countryTxt.delegate = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func cancelBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addBtnPressed(_ sender: Any) {
        let restData = ["name" : nameTxt.text!,
                        "city" : cityTxt.text!,
                        "country" : countryTxt.text!] as [String : Any]
        DataService.instance.createRestaurant(uid: String (Int.random(in: 0 ..< 30000)), restData: restData)
        
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddRestaurantViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == nameTxt) {
            cityTxt.becomeFirstResponder()
        }
        if (textField == cityTxt) {
            countryTxt.becomeFirstResponder()// change to login
        }
        if (countryTxt == cityTxt) {
            view.endEditing(true)// change to login
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
}
