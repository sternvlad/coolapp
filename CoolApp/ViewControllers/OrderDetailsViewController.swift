//
//  OrderDetailsViewController.swift
//  CoolApp
//
//  Created by Stern Eduard on 02/08/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import UIKit

class OrderDetailsViewController: UIViewController {

    var userId = ""
    var selectedRestaurantId = ""
    var ordersId = ""
    var productsOrder = [ProductOrder?] ()
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        selectedRestaurantId = UserDefaults.standard.string(forKey: "selectedRestaurantID") ?? ""
        userId = UserDefaults.standard.string(forKey: "userId") ?? ""
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DataService.instance.loadProductsByOrder(orderID: ordersId) { (productsOrder) in
            self.productsOrder = productsOrder
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    
}


extension OrderDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productsOrder.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell (style: .subtitle, reuseIdentifier: "Cell")
        let prod = productsOrder[indexPath.row]
        cell.textLabel?.text = prod?.name
        cell.detailTextLabel?.text = "Quantity: \(String(describing: prod!.quantity))"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
}

