//
//  OrderHistoryViewController.swift
//  CoolApp
//
//  Created by Stern Eduard on 31/07/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import UIKit

class OrderHistoryViewController: UIViewController {

    var userId = ""
    var selectedRestaurantId = ""
    var selectedOrderId = ""
    var orders : [Order?]?
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        selectedRestaurantId = UserDefaults.standard.string(forKey: "selectedRestaurantID") ?? ""
        userId = UserDefaults.standard.string(forKey: "userId") ?? ""

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DataService.instance.loadOrdersWithUserIdAndRestaurantId(userId: userId, restaurantId: selectedRestaurantId) { [weak self] (orders) in
            self?.orders = orders
            self?.tableView.reloadData()
        }
    }
    
    
    @IBAction func menuButtonDidClicked(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if let viewController = segue.destination as? OrderDetailsViewController {
            viewController.ordersId = selectedOrderId
        }
    }

}


extension OrderHistoryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell (style: .subtitle, reuseIdentifier: "Cell")
        let order = orders?[indexPath.row]
        cell.textLabel?.text = "Order date: \(String(describing: order!.date.toString(dateFormat: "YYYY-MM-DD hh:mm")))"
        cell.detailTextLabel?.text = "Order status: \(String(describing: order!.status))"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedOrderId = orders?[indexPath.row]?.orderId ?? ""
        performSegue(withIdentifier: "goToOrderDetails", sender: self)
    }
    
   
}

