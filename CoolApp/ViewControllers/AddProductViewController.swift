//
//  AddProductViewController.swift
//  CoolApp
//
//  Created by Stern Eduard on 31/07/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import UIKit

class AddProductViewController: UIViewController, Alertable {

    @IBOutlet weak var nameRestLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var restaurantIdLbl: UILabel!
    @IBOutlet weak var nameProdTxt: UITextField!
    @IBOutlet weak var quantityTxt: UITextField!
    @IBOutlet weak var suggestedOrderTxt: UITextField!
    @IBOutlet weak var sixWeTxt: UITextField!
    var selectedRestaurant: Restaurant?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()
        self.moveViewWhenKeyboardShows()
        
        // Do any additional setup after loading the view.
    }
    

    @IBAction func cancelBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addBtnPressed(_ sender: Any) {
        guard let name = nameProdTxt.text else {
            return
        }
        guard let quantity = quantityTxt.text else {
            return
        }
        guard let sugestedOrder = suggestedOrderTxt.text else {
            return
        }
        guard let sixWeCons = sixWeTxt.text else {
            return
        }
        if Int (quantity) == nil {
            showAlert("Quantity should be an Int number")
            return
        }
        if Int (sugestedOrder) == nil {
            showAlert("Sugested Order should be an Int number")
            return
        }
        if Int (sixWeCons) == nil {
            showAlert("Six weeks consumption should be an Int number")
            return
        }
        let prodData = ["name" : name,
                        "quantity" : quantity,
                        "sugestedOrder": sugestedOrder,
                        "6weCons": sixWeCons,
                        "restaurantId": selectedRestaurant?.id as Any] as [String : Any]
        
        DataService.instance.createProduct(prodData: prodData)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
