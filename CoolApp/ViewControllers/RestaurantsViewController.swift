//
//  RestaurantsViewController.swift
//  CoolApp
//
//  Created by Stern Eduard on 30/07/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import UIKit
import SideMenuSwift
import Firebase

class RestaurantsViewController: UIViewController {
    
    var restaurants: [Restaurant] = []
    @IBOutlet weak var btnLeft: UIBarButtonItem!
    @IBOutlet weak var btnRight: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    var role = "user"
    var selectedRestaurant: Restaurant?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SideMenuController.preferences.basic.menuWidth = 480
        setupTable ()
        role = UserDefaults.standard.string(forKey: "role") ?? ""
        if role == "admin" {
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(logoutBtnPressed))
        }else {
            navigationItem.rightBarButtonItem = nil
        }
    }
    
    @objc func logoutBtnPressed() {
        do {
            try Auth.auth().signOut()
        } catch (let error) {
            print (error)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupTable()
    }
    
    func setupTable () {
        DataService.instance.REF_RESTAURANTS.getDocuments { [weak self] (querySnapshot, error) in
            guard let strongSelf = self else {return}
            
            if let restaurantSnapshots = querySnapshot?.documents {
                strongSelf.restaurants.removeAll()
                for restaurant in restaurantSnapshots {
                    let restaurantName = restaurant.data()["name"] as! String
                    let restaurantCity = restaurant.data()["city"] as! String
                    let restaurantCountry = restaurant.data()["country"] as! String
                    strongSelf.restaurants.append(Restaurant (id: restaurant.documentID, name: restaurantName, city: restaurantCity, country: restaurantCountry))
                }
            }
            strongSelf.tableView.reloadData()
        }
    }
    
    @IBAction func menuButtonDidClicked(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToAddProduct" {
            if let viewController = segue.destination as? AddProductViewController {
                viewController.selectedRestaurant = selectedRestaurant
            }
        }
    }
}

extension RestaurantsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurants.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "restaurantTableViewCell") as! RestaurantTableViewCell
        let rest = restaurants[indexPath.row]
        cell.titleLabel.text = rest.name
        cell.locationLabel.text = "\(rest.country) -- \(rest.city)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (role == "admin") {
            selectedRestaurant = restaurants[indexPath.row];
            self.performSegue(withIdentifier: "goToAddProduct", sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if (role == "admin") {
            return true
        }else {
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let restaurant = restaurants [indexPath.row]
        if editingStyle == .delete {
            DataService.instance.REF_RESTAURANTS.document(restaurant.id).delete()
            restaurants.remove(at: indexPath.row)
            tableView.reloadData()
        }
    }
}
