//
//  DateExt.swift
//  CoolApp
//
//  Created by Stern Eduard on 02/08/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//
import Foundation

extension Date
{
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
}
