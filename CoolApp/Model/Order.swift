//
//  Order.swift
//  CoolApp
//
//  Created by Stern Eduard on 02/08/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import UIKit

struct Order {
    var orderId: String
    var restaurantId: String
    var userId: String
    var status: String
    var date: Date
    var products: [ProductOrder?]
    
    init(orderId: String, restaurantId: String, userId: String, status: String, products: [ProductOrder?], date: Date) {
        self.orderId = orderId
        self.restaurantId = restaurantId
        self.userId = userId
        self.status = status
        self.products = products
        self.date = date
    }
}
