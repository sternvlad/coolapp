//
//  Restaurant.swift
//  CoolApp
//
//  Created by Stern Eduard on 29/07/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import UIKit

struct Restaurant {
    var id: String
    var name: String
    var city: String
    var country: String
    
    init(id: String, name: String, city: String, country: String) {
        self.id = id
        self.name = name
        self.city = city
        self.country = country
    }
}
