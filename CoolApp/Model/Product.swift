//
//  Product.swift
//  CoolApp
//
//  Created by Stern Eduard on 30/07/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import Foundation

struct Product {
    var id: String
    var name: String
    var quantity: String
    var sixWeCons: String
    var restId: String
    var sugestedOrder: String
    var orderedQuantity: Int
    var productChecked: Bool
    
    init(id: String, name: String, quantity: String, sixWeCons: String, restId: String, sugestedOrder: String) {
        self.id = id
        self.name = name
        self.quantity = quantity
        self.sixWeCons = sixWeCons
        self.restId = restId
        self.sugestedOrder = sugestedOrder
        self.orderedQuantity = 0
        self.productChecked = false
    }
}

struct ProductOrder {
    var id: String
    var orderId: String
    var quantity: String
    var productId: String
    var name: String
    
    init(id: String, name: String, quantity: String, productId: String, orderId: String) {
        self.id = id
        self.name = name
        self.quantity = quantity
        self.productId = productId
        self.orderId = orderId
    }
}
